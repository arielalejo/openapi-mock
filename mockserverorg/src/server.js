var mockserver = require('mockserver-node');

var port = 1080;

mockserver
    .start_mockserver({
        serverPort: port,
        trace: true,
    })
    

mockserver.stop_mockserver({
    serverPort: port,
});
