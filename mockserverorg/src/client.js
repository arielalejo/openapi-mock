var client = require('mockserver-client').mockServerClient;

var port = 1080;

client('localhost', port)
    .mockAnyResponse(
        {
            httpRequest: {
                method: 'GET',
                path: '/testmock',
            },
            httpResponse: {
                statusCode: 200,
                body: JSON.stringify({
                    name: 'ariel',
                    age: 29,
                }),
            },
        },        
    )    
    .then(() => console.log('expectation created'))
    .catch((error) => console.log(error));

