'use strict';


/**
 * bind additional listening ports
 * only supported on Netty version
 *
 * body Ports list of ports to bind to, where 0 indicates dynamically bind to any available port
 * returns Ports
 **/
exports.bindPUT = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "ports" : [ 0.8008281904610115, 0.8008281904610115 ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * clears expectations and recorded requests that match the request matcher
 *
 * body HttpRequest request used to match expectations and recored requests to clear (optional)
 * no response value expected for this operation
 **/
exports.clearPUT = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * clears all expectations and recorded requests
 *
 * no response value expected for this operation
 **/
exports.resetPUT = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * retrieve either - recorded requests or - the active expectations
 *
 * body HttpRequest request used to match which recorded requests, expectations or log messages to return, an empty body matches all requests, expectations or log messages (optional)
 * format String changes response format, default if not specificed is \"json\", supported values are \"java\", \"json\" (optional)
 * type String specifies the type of object that is retrieve, default if not specified is \"requests\", supported values are \"requests\", \"recorded_expectations\", \"active_expectations\" (optional)
 * returns inline_response_200
 **/
exports.retrievePUT = function(body,format,type) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * return listening ports
 *
 * returns Ports
 **/
exports.statusPUT = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "ports" : [ 0.8008281904610115, 0.8008281904610115 ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * stop running process
 * only supported on Netty version
 *
 * no response value expected for this operation
 **/
exports.stopPUT = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

