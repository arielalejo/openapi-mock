'use strict';


/**
 * verify a request has been received a specific number of times
 *
 * body Verification request matcher and the number of times to match
 * no response value expected for this operation
 **/
exports.verifyPUT = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * verify a sequence of request has been received in the specific order
 *
 * body VerificationSequence the sequence of requests matchers
 * no response value expected for this operation
 **/
exports.verifySequencePUT = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

