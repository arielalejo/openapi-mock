'use strict';

var utils = require('../utils/writer.js');
var Expectation = require('../service/ExpectationService');

module.exports.expectationPUT = function expectationPUT (req, res, next, body) {
  Expectation.expectationPUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
