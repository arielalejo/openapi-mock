'use strict';

var utils = require('../utils/writer.js');
var Control = require('../service/ControlService');

module.exports.bindPUT = function bindPUT (req, res, next, body) {
  Control.bindPUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.clearPUT = function clearPUT (req, res, next, body) {
  Control.clearPUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.resetPUT = function resetPUT (req, res, next) {
  Control.resetPUT()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.retrievePUT = function retrievePUT (req, res, next, body, format, type) {
  Control.retrievePUT(body, format, type)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.statusPUT = function statusPUT (req, res, next) {
  Control.statusPUT()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.stopPUT = function stopPUT (req, res, next) {
  Control.stopPUT()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
