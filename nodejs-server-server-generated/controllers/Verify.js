'use strict';

var utils = require('../utils/writer.js');
var Verify = require('../service/VerifyService');

module.exports.verifyPUT = function verifyPUT (req, res, next, body) {
  Verify.verifyPUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.verifySequencePUT = function verifySequencePUT (req, res, next, body) {
  Verify.verifySequencePUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
